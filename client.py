import socket  
import time            

s = socket.socket()        
port = 201               
s.connect(('127.0.0.1', port))
while True:
    time.sleep(0.2)
    client = s.recv(1024).decode()
    print(client)
    s.send(str(int(client)+1).encode())
    if int(client) == 99:
        break
s.close